import express = require("express");
import fs = require("fs");
import { isA, Schema, FieldType as FT } from "blue-scheme";
import { Users } from "./users";
import bcrypt = require("bcrypt");

const options = JSON.parse(fs.readFileSync(`./options.json`, { encoding: "utf8" }));

export function makeAuth(users: Users) {
    return function(req: express.Request, res: express.Response, next: express.NextFunction): any {
        if (!isA(FT.string).validate(req.body.username).isValid) return res.status(401).json({
            success: false,
            message: "Invalid username"
        });

        if (!isA(FT.string).validate(req.body.password).isValid) return res.status(401).json({
            success: false,
            message: "Invalid password"
        });

        users.getUser(req.body.username)
            .then(user => bcrypt.compare(req.body.password, user.passwordHash))
            .then(result => {
                if (!result) return res.status(403).json({
                    success: false,
                    message: "Invalid password"
                });

                next();
            }).catch(() => res.status(403).json({
                success: false,
                message: "Invalid username"
            }));
    }
}