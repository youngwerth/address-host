import express = require("express");
import bodyParser = require("body-parser");
import bcrypt = require("bcrypt");
import Recaptcha = require("express-recaptcha");
import Handlebars = require("handlebars");
import fs = require("fs");
import { Users, User } from "./users";
import { makeAuth } from "./auth";
import { UserSchema, UpdateAddressSchema } from "./schema";

const options = JSON.parse(fs.readFileSync(`./options.json`, { encoding: "utf8" }));
const saltRounds = options.saltRounds || 10;

const users = new Users();

const app = express();
const recaptcha = new Recaptcha(options.recaptchaSiteKey, options.recaptchaSecretKey);
const auth = makeAuth(users);

const addressTemplate = Handlebars.compile(fs.readFileSync(`${__dirname}/../views/address.hbs`, { encoding: "utf8" }))

app.use(bodyParser.json());
app.use(express.static(`${__dirname}/../public`));
app.use(bodyParser.urlencoded({ extended: true }));

app.get("/addresses/:username", async (req, res) => {
    let user: User;
    try {
        user = await users.getUser(req.params.username);
    } catch (error) { return res.status(404).redirect("/not-found.html"); }

    res.send(addressTemplate(user));
});

app.post("/api/create-account", recaptcha.middleware.verify, async (req, res) => {
    if (options.recaptcha && (req as any).recaptcha.error) return res.status(400).json({
        success: false,
        message: "Failed recaptcha"
    });

    const reuslt = UserSchema.validate(req.body);
    if (!reuslt.isValid) return res.status(400).json({
        success: false,
        message: "Form has invalid fields"
    });

    let passwordHash: string;
    try {
        passwordHash = await bcrypt.hash(req.body.password, saltRounds);
    } catch (e) {
        return res.sendStatus(500);
    }

    try {
        await users.addUser({
            username: req.body.username,
            passwordHash,
            email: req.body.email,
            iotaAddress: req.body.iotaAddress
        });
    } catch (e) {
        return res.status(400).json({
            success: false,
            message: "Username taken"
        });
    }

    res.status(201).redirect("/success.html");
});

app.post("/api/update-address", recaptcha.middleware.verify, auth, async (req, res) => {
    if (options.recaptcha && (req as any).recaptcha.error) return res.status(400).json({
        success: false,
        message: "Failed recaptcha"
    });

    const result = UpdateAddressSchema.validate(req.body);
    if (!result.isValid) return res.status(400).json({
        success: false,
        message: "Form contains invalid fields"
    });

    try {
        await users.updateAddress(req.body.username, req.body.iotaAddress);
    } catch (e) {
        return res.status(400).json({
            success: false,
            message: e
        });
    }

    res.status(200).redirect("/success.html");
});

app.get("*", function (req, res) {
    return res.redirect("/not-found.html");
});

export = app;
