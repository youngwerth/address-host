import { Depot } from "depot-db";

export interface User {
    username: string,
    email?: string,
    passwordHash: string,
    iotaAddress: string
}

export class Users {
    private readonly db = new Depot<string, User>(`${__dirname}/../users.db`);

    async addUser(user: User) {
        const exists: boolean = await this.db.get(user.username).then(() => true).catch(() => false);
        if (exists) return Promise.reject("Username taken");
        return this.db.put(user.username, user);
    }

    async getUser(username: string): Promise<User> {
        return this.db.get(username);
    }

    async updateAddress(username: string, address: string) {
        const user = await this.db.get(username);
        user.iotaAddress = address;
        return this.db.put(username, user);
    }
}