import fs = require("fs");
import createServer = require("auto-sni");
import app = require("./app");

const options = JSON.parse(fs.readFileSync(`./options.json`, { encoding: "utf8" }));

if (options.autoHTTPS) {
    const server = createServer(options.autoHTTPSOpts, app);
} else {
    app.listen(9000).on("listening", () => console.log("Listening on port 9000"));
}

