import { isA, Schema, FieldType as FT } from "blue-scheme";

export const UserSchema = new Schema({
    username: isA(FT.string, {
        min: 1,
        max: 50,
        regex: /^[A-Za-z0-9\-]+$/
    }),
    password: isA(FT.string, {
        min: 1,
        max: 100,
    }),
    email: isA(FT.string, {
        optional: true,
        min: 3,
        max: 150,
        regex: /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i
    }),
});

export const UpdateAddressSchema = new Schema({
    username: isA(FT.string, {
        min: 1,
        max: 50,
        regex: /^[A-Za-z0-9\-]+$/
    }),
    password: isA(FT.string, {
        min: 1,
        max: 100,
    }),
    iotaAddress: isA(FT.string, {
        min: 1,
        max: 4000,
        regex: /^([A-Z]|9)+$/
    })
});