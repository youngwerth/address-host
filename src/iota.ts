import IOTA = require("iota.lib.js");

export function newIota(): IOTA {
    const iota = new IOTA({
        provider: "http://eugene.iotasupport.com:14999"
    });

    return iota;
}