
// Type definitions for vorpal 1.11.4
// Project: https://github.com/dthree/vorpal
// Definitions by: Jim Buck <http://github.com/jimmyboh>
// Definitions: https://github.com/DefinitelyTyped/DefinitelyTyped

declare module "vorpal" {
    import {} from 'inquirer';
    import { ParsedArgs } from 'minimist';

    type CallbackFunction<T> = (err?: string | Error, data?: T) => void;

    interface TypesDefinition
    {
        string?: string[];
        number?: string[];
        // TODO: Check other types.
    }

    function VorpalFactory(): VorpalInstance;

    interface VorpalInstance {

        ui: UiInstance;

        /**
         * Parses the process's `process.argv` arguments and executes the matching command.
         *
         * @param {(string|string[])} argv
         * @param {{use?: 'minimist'}} [options] When `use: 'minimist'` is passed in as an option, `.parse` will instead expose the `minimist` module's main method and return the results, executing no commands.
         */
        parse(argv: string | string[], options?: { use?: 'minimist' }): ParsedArgs;

        /**
         * Sets the prompt delimiter for the given Vorpal instance.
         *
         * @param {string} str
         * @returns {this}
         */
        delimiter(str: string): this;

        /**
         * Attaches the TTY's CLI prompt to that given instance of Vorpal.
         * As a note, multiple instances of Vorpal can run in the same Node instance. However, only one can be 'attached' to your TTY. The last instance given the show() command will be attached, and the previously shown instances will detach.
         *
         * @returns {this}
         */
        show(): this;

        /**
         * Returns a given command by its name. This is used instead of `vorpal.command()` as `.command` will overwrite a given command. If command is not found, `undefined` is returned.
         *
         * @param {string} name
         * @returns {Command}
         */
        find(name: string): this;

        exec<T>(command: string, cb: CallbackFunction<T>): PromiseLike<T>;

        execSync<T>(command: string, options: { fatal: boolean }): T;

        log(message: string, ...messages: string[]): void;

        history(id: string): void;

        localStorage(id: string): void;

        help(strBuilder: (cmd: string) => string): void;

        pipe(pipeFn: (stdout: string) => string): void;

        use(extension: string | Function): void;

        command(command: string, description?: string): this;

        catch(command: string, description?: string): this;

        mode(command: string, description?: string): Mode;

        description(description: string): this;
        alias(name: string): this;
        alias(...names: string[]): this;
        option(flag: string, description: string, autocomplete: string[]): this; // TODO: Check autocomplete types.
        types(types: TypesDefinition): this;
        hidden(): this; // TODO: Check return type.
        remove(): void; // TODO: Check return type.
        help(helpFn: (...args: any[]) => void): void; // TODO: Check args type.
        autocomplete(choices: string[]): this;
        autocomplete(choices: {}): this; // TODO: Revisit this.
        action(this: this, actionFn: (args: any, cb: () => void) => void): this;
    }

    interface UiInstance {

        redraw: RedrawMethod;

        delimiter(text?: string): void;

        input(text?: string): void;

        imprint(): void;

        submit(text: string): void;

        cancel(): void;
    }

    function RedrawMethod(text: string): void;
    function RedrawMethod(...texts: string[]): void;

    interface RedrawMethod {
        clear(): void;
        done(): void;
    }

    interface Mode {
        description(desc: string): this;

        delimiter(str: string): this;

        init(initFn: (args: any, callback: CallbackFunction<void>) => void): this;

        action<T>(actionFn: (command: string, callback: CallbackFunction<void | T>) => void | T): this;
    }

    export = VorpalFactory;
}
