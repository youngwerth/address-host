declare module "express-recaptcha" {
    import { Express } from "express";

    class Middleware {
        verify(...args: any[]): void;
    }

    export = class Recaptcha {
        readonly middleware: Middleware;

        constructor(siteKey: string, secretKey: string);

        verify(req: Express.Request, cb: (err: Error, data: any) => void): void;
    }
}