declare module "iota.lib.js" {

    export = IOTA;

    namespace IOTA {
        type TransactionObject = {
            hash: string,
            signatureMessageFragment: string,
            address: string,
            value: number,
            obsoleteTag: string,
            timestamp: number,
            currentIndex: number,
            lastIndex: number,
            bundle: string,
            trunkTransaction: string,
            branchTransaction: string,
            tag: string,
            attachmentTimestamp: number,
            attachmentTimestampLowerBound: number,
            attachmentTimestampUpperBound: number,
            nonce: string
        };

        type TransferObject = {
            address: string,
            value: number,
            message?: string,
            tag?: string,
        };

        type Inputs = {
            address: string,
            balance: number,
            keyIndex: number,
            security: 1 | 2 | 3,
        }[];

        type AccountData = {
            latestAddress: string, // Latest, unused address which has no transactions in the tangle
            addresses: string[], // List of all used addresses which have transactions associated with them
            transfers: {
                address: string,
                value: number,
                message: string,
                tag: string,
            }[], // List of all transfers associated with the addresses
            inputs: Inputs, // List of all inputs available for the seed. Follows the getInputs format of `address`, `balance`, `security` and `keyIndex`
            balance: number // latest confirmed balance
        };

        type Unit = "i" | "Ki" | "Mi" | "Gi" | "Ti" | "Pi";

        class API {
            attachToTangle(...args: any[]): void;
            /**
             * Wrapper function for getTrytes and the Utility function transactionObjects. This function basically returns the entire transaction objects for a list of transaction hashes.
             * @param hashes - List of transaction hashes
             * @param cb - Callback
             */
            getTransactionsObjects(hashes: string[], cb: (err: Error | undefined, transactions: TransactionObject[]) => void): void;

            /**
             * Wrapper function for findTransactions, getTrytes and the Utility function transactionObjects. This function basically returns the entire transaction objects for a list of key values which you would usually use for findTransactions
             * @param searchValues
             * @param cb
             */
            findTransactionObjects(searchValues: {
                bundles?: string[],
                addresses?: string[],
                tags?: string[],
                approvees?: string[]
            }[], cb: (err: Error | undefined, transactionObjects: TransactionObject[]) => void): void;

            /**
             * Wrapper function for getNodeInfo and getInclusionStates. It simply takes the most recent solid milestone as returned by getNodeInfo, and uses it to get the inclusion states of a list of transaction hashes.
             */
            getLatestInclusion(hashes: string[], cb: (err: Error | undefined, inclusions: any[]) => void): void;

            /**
             * Wrapper function for broadcastTransactions and storeTransactions
             */
            storeAndBroadcast(trytes: any[], cb: (err: Error | undefined, success: any) =>void): void;

            /**Generates a new address from a seed and returns the address. This is either done deterministically, or by providing the index of the new address to be generated. When generating an address, you have the option to choose different security levels for your private keys. A different security level with the same key index, means that you will get a different address obviously (as such, you could argue that single seed has 3 different accounts, depending on the security level chosen). */
            getNewAddress(seed: string, options: {
                index?: number,
                checksum?: boolean,
                total?: number,
                security?: 1 | 2 | 3,
                returnAll?: boolean
            }, cb: (err: Error | undefined, address: string | string[]) => void): void;
            getNewAddress(seed: string, cb: (err: Error | undefined, address: string | string[]) => void): void;

            /**
             * Gets all possible inputs of a seed and returns them with the total balance. This is either done deterministically (by genearating all addresses until findTransactions returns null for a corresponding address), or by providing a key range to use for searching through.
             */
            getInputs(seed: string, options: {
                start: number,
                end: number,
                security: 1 | 2 | 3,
                threshold: number,
            }, cb: (err: Error | undefined, balance: {
                inputs: {
                    address: string,
                    balance: number,
                    keyIndex: number
                }[],
                totalBalance: number
            }) => void): void;

            sendTrytes(trytes: string[], depth: number, minWeightMagnitude: number, cb: (err: Error | undefined, transactionObjects: TransactionObject[]) => void): void;

            /** Wrapper function that basically does prepareTransfers, as well as attachToTangle and finally, it broadcasts and stores the transactions locally. */
            sendTransfer(seed: string, depth: number, minWeightMagnitude: number, transfers: TransferObject[] | TransferObject, options: {
                inputs?: Inputs,
                address: string,
            }, cb: (err: Error) => void): void;
            sendTransfer(seed: string, depth: number, minWeightMagnitude: number, transfers: TransferObject[] | TransferObject, cb: (err: Error) => void): void;

            /** Similar to getTransfers, just a bit more comprehensive in the sense that it also returns the addresses, transfers, inputs and balance that are associated and have been used with your account (seed). This function is useful in getting all the relevant information of your account. If you want to have your transfers split into received / sent, you can use the utility function categorizeTransfers */
            getAccountData(seed: string, options: {
                start?: number,
                end?: number,
                security?: 1 | 2 | 3
            }, cb: (err: Error | undefined, data: AccountData) => void): void;
            getAccountData(seed: string, cb: (err: Error | undefined, data: AccountData) => void): void;
        }

        class Util {
            /** IOTA utilizes the Standard system of Units. See below for all available units */
            convertUnits(value: number | string, fromUnit: | Unit, toUnit: Unit): number;

            /** Takes a tryte-encoded input value and adds a checksum (length is user defined). Standard checksum length is 9 trytes. If isAddress is defined as true, it will validate if it's a correct 81-tryte enocded address. */
            addChecksum(inputValue: string | string[], checksumLength?: number, isAddress?: boolean): string | string[];

            /** Takes an 90-trytes address as input and simply removes the checksum. */
            noChecksum(address: string | string[]): string | string[];

            /** Takes an 90-trytes checksummed address and returns a true / false if it is valid. */
            isValidChecksum(addressWithChecksum: string): boolean;

            /** Converts the trytes of a transaction into its transaction object. */
            transactionObject(trytes: string): TransactionObject;

            /** Converts a valid transaction object into trytes. Please refer to [TODO] for more information what a valid transaction object looks like. */
            transactionTrytes(transactionObject: TransactionObject): string;

            toTrytes(input: string): string;
            fromTrytes(trytes: string): string;

            /** This function makes it possible for each of the co-signers in the multi-signature to independently verify that a generated transaction with the corresponding signatures of the co-signers is valid. This function is safe to use and does not require any sharing of digests or key values. */
            validateSignatures(signedBundle: any[], inputAddress: string): boolean;
        }

        class Multisig {
            /** Generates the corresponding private key (depending on the security chosen) of a seed. */
            getKey(seed: string, index: number, security: 1 | 2 | 3): string;
        }
    }

    class IOTA {
        /** Current version of the library */
        readonly version: string;
        readonly api: IOTA.API;
        readonly multisig: IOTA.Multisig;
        readonly utils: IOTA.Util;

        constructor(options?: {
            /** Host you want to connect to. Can be DNS, IPv4 or IPv6. Defaults to localhost */
            host?: string,
            /** Port of the host you want to connect to. Defaults to 14265 */
            port?: number,
            /** If you don't provide host and port, you can supply the full provider value to connect to */
            provider?: string,
            /** Optional value to determine if your provider is the IOTA Sandbox or not */
            sandbox?: boolean
        });
    }

}