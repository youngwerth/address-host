# Address Host

Address Host Button provides a single location to publish your latest
public crypto-currency address.

## Building

Requirements:
- node v6+

```bash
git clone git@gitlab.com:youngwerth/address-host.git
cd address-host
npm install
npm run build
```